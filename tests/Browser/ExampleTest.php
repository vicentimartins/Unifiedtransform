<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class ExampleTest extends DuskTestCase
{
    /** @test */
    public function itShowsAppName()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('UnifiedTransform');
        });
    }
}
