<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\SettingPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AdminUserManagesAcademicSettingsTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
        $this->admin = factory(User::class)->states('admin')->create();
    }

    /** @test */
    public function adminUserCanSeeAcademicSettings()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->admin)
                ->visit(new SettingPage());
        });
    }
}
