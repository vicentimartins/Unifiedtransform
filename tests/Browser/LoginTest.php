<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function userMasterCanSignIn()
    {
        $user = factory(User::class)->states('master')->create();

        $this->browse(function ($browser) use ($user) {
            $browser->visit('/login')
                    ->waitForText('Login')
                    ->type('email', $user->email)
                    ->type('password', 'secret')
                    ->press('Login')
                    ->assertPathIs('/masters');
        });
    }
}
