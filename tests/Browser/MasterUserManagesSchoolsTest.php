<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\MasterPage;
use Tests\Browser\Pages\SchoolPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class MasterUserManagesSchoolsTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
        $this->master = factory(User::class)->states('master')->create();
    }

    /** @test */
    public function masterUserCanSeeListSchools()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->master)
                ->visit(new MasterPage())
                ->clickLink('Manage Schools')
                ->assertSee('School List');
        });
    }

    /** @test */
    public function masterUserCreatesSchool()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->master)
                ->visit(new SchoolPage())
                ->pause(1000)
                ->click('@create-school-button')
                ->pause(1000)
                ->createSchool('Benito Juárez')
                ->assertSee('Benito Juárez');
        });
    }

    /** @test */
    public function masterUserUpdatesSchool()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs($this->master)
                    ->visit(new SchoolPage())
                    ->pause(1000)
                    ->click('@edit-school-link')
                    ->pause(1000)
                    ->updateSchool($this->master->school_id, 'New name')
                    ->assertSee('New name');
        });
    }
}
