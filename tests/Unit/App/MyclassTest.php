<?php

namespace Tests\Unit\App;

use App\School;
use App\Myclass;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MyclassTest extends TestCase
{
    use RefreshDatabase;

    protected $class;

    public function setUp(): void
    {
        parent::setUp();
        $this->class = create(Myclass::class);
    }

    /** @test */
    public function aClassIsAnInstanceOfMyclass()
    {
        $this->assertInstanceOf('App\Myclass', $this->class);
    }

    /** @test */
    public function aClassBelongsToSchool()
    {
        $this->assertInstanceOf('App\School', $this->class->school);
    }

    /** @test */
    public function aClassHasSections()
    {
        $this->assertInstanceOf(
            'Illuminate\Database\Eloquent\Collection', $this->class->sections
        );
    }

    /** @test */
    public function aClassHasBooks()
    {
        $this->assertInstanceOf(
            'Illuminate\Database\Eloquent\Collection', $this->class->books
        );
    }

    /** @test */
    public function myClassAreFilterBySchool()
    {
        $school = create(School::class);
        $klass = create(Myclass::class, ['school_id' => $school->id], 2);

        $other_school = create(School::class);
        $other_klass = create(Myclass::class, ['school_id' => $other_school->id], 4);

        $this->assertEquals(Myclass::bySchool($school->id)->count(), $klass->count());
    }
}
