<?php

namespace Tests\Unit\App;

use App\Attendance;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AttendanceTest extends TestCase
{
    use RefreshDatabase;

    protected $attendance;

    public function setUp(): void
    {
        parent::setUp();
        $this->attendance = create(Attendance::class);
    }

    /** @test */
    public function anAttendanceIsAnInstanceOfAttendance()
    {
        $this->assertInstanceOf('App\Attendance', $this->attendance);
    }

    /** @test */
    public function anAttendanceBelongsToStudent()
    {
        $this->assertInstanceOf('App\User', $this->attendance->student);
    }

    /** @test */
    public function anAttendanceBelongsToSection()
    {
        $this->assertInstanceOf('App\Section', $this->attendance->section);
    }

    /** @test */
    public function anAttendanceBelongsToExam()
    {
        $this->assertInstanceOf('App\Exam', $this->attendance->exam);
    }
}
