<?php

namespace Tests\Unit\App;

use App\Section;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SectionTest extends TestCase
{
    use RefreshDatabase;

    protected $section;

    public function setUp(): void
    {
        parent::setUp();
        $this->section = create(Section::class);
    }

    /** @test */
    public function aSectionIsAnInstanceOfSection()
    {
        $this->assertInstanceOf('App\Section', $this->section);
    }

    /** @test */
    public function aSectionBelongsToClass()
    {
        $this->assertInstanceOf('App\Myclass', $this->section->class);
    }
}
