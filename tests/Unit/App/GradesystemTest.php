<?php

namespace Tests\Unit\App;

use App\School;
use Tests\TestCase;
use App\Gradesystem;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GradesystemTest extends TestCase
{
    use RefreshDatabase;

    protected $gradesystem;

    public function setUp(): void
    {
        parent::setUp();
        $this->gradesystem = create(Gradesystem::class);
    }

    /** @test */
    public function aGradesystemIsAnInstanceOfGradesystem()
    {
        $this->assertInstanceOf('App\Gradesystem', $this->gradesystem);
    }

    /** @test */
    public function aGradesystemBelongsToSchool()
    {
        $this->assertInstanceOf('App\School', $this->gradesystem->school);
    }

    /** @test */
    public function theGradesystemsAreFilterBySchool()
    {
        $school = create(School::class);
        $gradesystems = create(Gradesystem::class, ['school_id' => $school->id], 2);

        $other_school = create(School::class);
        $other_gradesystems = create(Gradesystem::class, ['school_id' => $other_school->id], 4);

        $this->assertEquals(Gradesystem::bySchool($school->id)->count(), $gradesystems->count());
    }
}
