<?php

namespace Tests\Unit\App;

use App\School;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SchoolTest extends TestCase
{
    use RefreshDatabase;

    protected $school;

    public function setUp(): void
    {
        parent::setUp();
        $this->school = create(School::class);
    }

    /** @test */
    public function aSchoolIsAnInstanceOfSchool()
    {
        $this->assertInstanceOf('App\School', $this->school);
    }

    /** @test */
    public function aSchoolHasUsers()
    {
        $this->assertInstanceOf(
            'Illuminate\Database\Eloquent\Collection', $this->school->users
        );
    }

    /** @test */
    public function aSchoolHasDepartments()
    {
        $this->assertInstanceOf(
            'Illuminate\Database\Eloquent\Collection', $this->school->departments
        );
    }
}
