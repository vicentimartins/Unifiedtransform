<?php

namespace Tests\Unit\App;

use App\Book;
use App\School;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookTest extends TestCase
{
    use RefreshDatabase;

    protected $book;

    public function setUp(): void
    {
        parent::setUp();
        $this->book = create(Book::class);
    }

    /** @test */
    public function aClassIsAnInstanceOfBook()
    {
        $this->assertInstanceOf('App\Book', $this->book);
    }

    /** @test */
    public function aBookBelongsToSchool()
    {
        $this->assertInstanceOf('App\School', $this->book->school);
    }

    /** @test */
    public function aBookBelongsToClass()
    {
        $this->assertInstanceOf('App\Myclass', $this->book->class);
    }

    /** @test */
    public function aBookBelongsToUser()
    {
        $this->assertInstanceOf('App\User', $this->book->user);
    }

    /** @test */
    public function theBooksAreFilterBySchool()
    {
        $school = create(School::class);
        $books = create(Book::class, ['school_id' => $school->id], 2);

        $other_school = create(School::class);
        $other_books = create(Book::class, ['school_id' => $other_school->id], 4);

        $this->assertEquals(Book::bySchool($school->id)->count(), $books->count());
    }
}
