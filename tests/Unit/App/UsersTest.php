<?php

namespace Tests\Unit\App;

use App\User;
use App\School;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = create(User::class);
    }

    /** @test */
    public function anUserIsAnInstanceOfUser()
    {
        $this->assertInstanceOf('App\User', $this->user);
    }

    /** @test */
    public function anUserBelongsToSection()
    {
        $this->assertInstanceOf('App\Section', $this->user->section);
    }

    /** @test */
    public function anUserBelongsToSchool()
    {
        $this->assertInstanceOf('App\School', $this->user->school);
    }

    /** @test */
    public function anUserBelongsToDepartment()
    {
        $this->assertInstanceOf('App\Department', $this->user->department);
    }

    /** @test */
    public function anUserHasRole()
    {
        $accountant = factory(User::class)->states('accountant')->create();
        $admin = factory(User::class)->states('admin')->create();
        $librarian = factory(User::class)->states('librarian')->create();
        $master = factory(User::class)->states('master')->create();
        $student = factory(User::class)->states('student')->create();
        $teacher = factory(User::class)->states('teacher')->create();

        $this->assertTrue($accountant->hasRole('accountant'));
        $this->assertTrue($admin->hasRole('admin'));
        $this->assertTrue($librarian->hasRole('librarian'));
        $this->assertTrue($master->hasRole('master'));
        $this->assertTrue($student->hasRole('student'));
        $this->assertTrue($teacher->hasRole('teacher'));
    }

    /** @test */
    public function theUsersAreFilterBySchool()
    {
        $school = create(School::class);
        $users = create(User::class, ['school_id' => $school->id], 2);

        $other_school = create(School::class);
        $other_users = create(User::class, ['school_id' => $other_school->id], 4);

        $this->assertEquals(User::bySchool($school->id)->count(), $users->count());
    }
}
