<?php

namespace Tests\Unit\App;

use App\School;
use App\Issuedbook;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IssuedBookTest extends TestCase
{
    use RefreshDatabase;

    protected $issuedbook;

    public function setUp(): void
    {
        parent::setUp();
        $this->issuedbook = create(Issuedbook::class);
    }

    /** @test */
    public function anIssuedbookIsAnInstanceOfIssuedbook()
    {
        $this->assertInstanceOf('App\Issuedbook', $this->issuedbook);
    }

    /** @test */
    public function anIssuedbookBelongsToBook()
    {
        $this->assertInstanceOf('App\Book', $this->issuedbook->book);
    }

    /** @test */
    public function theIssuedBooksAreFilterBySchool()
    {
        $school = create(School::class);
        $issues = create(Issuedbook::class, ['school_id' => $school->id], 2);

        $other_school = create(School::class);
        $other_issues = create(Issuedbook::class, ['school_id' => $other_school->id], 4);

        $this->assertEquals(Issuedbook::bySchool($school->id)->count(), $issues->count());
    }
}
