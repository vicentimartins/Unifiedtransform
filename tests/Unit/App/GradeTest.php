<?php

namespace Tests\Unit\App;

use App\Grade;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GradeTest extends TestCase
{
    use RefreshDatabase;

    protected $grade;

    public function setUp(): void
    {
        parent::setUp();
        $this->grade = create(Grade::class);
    }

    /** @test */
    public function aGradeIsAnInstanceOfGrade()
    {
        $this->assertInstanceOf('App\Grade', $this->grade);
    }

    /** @test */
    public function aGradeBelongsToCourse()
    {
        $this->assertInstanceOf('App\Course', $this->grade->course);
    }

    /** @test */
    public function aGradeBelongsToStudent()
    {
        $this->assertInstanceOf('App\User', $this->grade->student);
    }

    /** @test */
    public function aGradeBelongsToTeacher()
    {
        $this->assertInstanceOf('App\User', $this->grade->teacher);
    }

    /** @test */
    public function aGradeBelongsToExam()
    {
        $this->assertInstanceOf('App\Exam', $this->grade->exam);
    }
}
