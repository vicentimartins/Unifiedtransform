<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use App\Certificate;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CertificateTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $admin = factory(User::class)->states('admin')->create();
        $this->actingAs($admin);
        $this->withoutExceptionHandling();
    }

    /** @test */
    public function adminCanViewUploadCertificatePage()
    {
        factory(User::class)->states('student')->create();
        $certificates = factory(Certificate::class, 50)->create();
        $response = $this->get('academic/certificate');
        $response->assertStatus(200)
                ->assertViewIs('certificates.create')
                ->assertViewHas('certificates');
    }

    /** @test */
    public function studentCanViewCertificates()
    {
        factory(User::class)->states('student')->create();
        $certificates = factory(Certificate::class, 50)->create();
        $response = $this->get('academic/student/certificates');
        $response->assertStatus(200)
                ->assertViewIs('certificates.index')
                ->assertViewHas('certificates');
    }
}
