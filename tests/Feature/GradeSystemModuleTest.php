<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use App\Gradesystem;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GradeSystemModuleTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $admin = factory(User::class)->states('admin')->create();
        $this->actingAs($admin);
        $this->withoutExceptionHandling();
    }

    /** @test */
    public function viewIs()
    {
        $response = $this->get('gpa/all-gpa');
        $response->assertStatus(200);
        $response->assertViewIs('gpa.all');
        $response->assertViewHas('gpas');
    }

    /** @test */
    public function adminCanViewGradeSystemCreateForm()
    {
        $response = $this->get('gpa/create-gpa');
        $response->assertStatus(200);
    }

    /** @test */
    public function adminCanCreateGradeSystem()
    {
        $gradesystem = factory(Gradesystem::class)->make();
        $this->followingRedirects()->post('create-gpa', $gradesystem->toArray())
            ->assertStatus(200);

        $this->assertDatabaseHas('grade_systems', $gradesystem->toArray());
    }

    /** @test */
    public function adminCanDeleteGradeSystem()
    {
        $gradesystem = factory(Gradesystem::class)->create();
        $this->followingRedirects()
            ->post('gpa/delete', [
                'gpa_id' => $gradesystem->id,
            ])
            ->assertStatus(200);
    }
}
