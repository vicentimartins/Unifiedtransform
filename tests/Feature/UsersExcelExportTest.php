<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersExcelExportTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $admin = factory(User::class)->states('admin')->create();
        $this->actingAs($admin);
        $this->withoutExceptionHandling();
    }

    /**
     * @test
     *
     * @return void
     */
    public function adminCanDownloadStudentsList()
    {
        $year = now()->year;

        $this->get('users/export/students-xlsx', ['year' => $year, 'type' => 'student'])
            ->assertStatus(200);
    }

    /**
     * @test
     *
     * @return void
     */
    public function adminCanDownloadTeachersList()
    {
        $year = now()->year;

        $this->get('users/export/students-xlsx', ['year' => $year, 'type' => 'teacher'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function nonAdminUsersCanNotSeeExportUsersForms()
    {
        $librarian = factory(User::class)->states('librarian')->create();
        $this->actingAs($librarian);

        $response = $this->get(url('/users', [$librarian->school_id, '1/0']));
        $response->assertDontSee('Export in Excel by Year');
    }
}
