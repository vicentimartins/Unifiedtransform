<?php

namespace Tests\Feature\Manage;

use App\User;
use App\Section;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SectionModuleTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $admin = factory(User::class)->states('admin')->create();
        $this->actingAs($admin);
        $this->withoutExceptionHandling();
    }

    /** @test */
    public function viewIs()
    {
        $this->get('school/sections')
            ->assertViewIs('school.sections');
    }

    /** @test */
    public function itShowsTheSectionList()
    {
        $this->get('school/sections')
            ->assertStatus(200)
            ->assertViewHas('sections');
    }

    /** @test */
    public function adminCanCreateSection()
    {
        $section = factory(Section::class)->make();
        $this->followingRedirects()
            ->post('school/add-section', $section->toArray())
            ->assertStatus(200);

        $this->assertDatabaseHas('sections', $section->toArray());

        $this->get('settings')
            ->assertSee('Section '.$section['section_number']);
    }
}
