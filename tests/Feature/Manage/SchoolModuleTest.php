<?php

namespace Tests\Feature\Manage;

use App\User;
use App\School;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SchoolModuleTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $master = factory(User::class)->states('master')->create();
        $this->actingAs($master);
        $this->withoutExceptionHandling();
    }

    /** @test */
    public function itShowsSchoolsList()
    {
        $this->get(route('schools.index'))
           ->assertStatus(200)
            ->assertViewIs('schools.index');
    }

    /** @test */
    public function itCreatesANewSchool()
    {
        $school = make(School::class);

        $this->post(route('schools.store'), $school->toArray())
            ->assertRedirect(route('schools.index'));
    }

    /** @test */
    public function itShowsEditSchool()
    {
        $school = create(School::class);

        $this->get(route('schools.edit', $school))
            ->assertStatus(200)
            ->assertViewIs('schools.edit');
    }

    /** @test */
    public function aSchoolCanBeingEdited()
    {
        $school = create(School::class, ['name' => 'Benito Juárez']);

        $school->name = 'New name';

        $this->from(route('schools.edit', $school->id))
            ->put(route('schools.update', $school->id), $school->toArray())
            ->assertRedirect(route('schools.index'));
    }
}
