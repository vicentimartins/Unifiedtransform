<?php

namespace Tests\Feature;

use App\Book;
use App\User;
use App\Issuedbook;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LibrarianTest extends TestCase
{
    use RefreshDatabase;

    protected $librarian;

    public function setUp(): void
    {
        parent::setUp();
        $this->librarian = factory(User::class)->states('librarian')->create();
        $this->actingAs($this->librarian);
        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function canSeeDashboardAsAHomePageAfterLogin()
    {
        $response = $this->get('/home');

        $response->assertStatus(200)
                ->assertSeeText('Dashboard');
    }

    /**
     * @test
     */
    public function canSeeListOfStudents()
    {
        $student = factory(User::class)->states('student')->create();
        $response = $this->get(url('/users', [$this->librarian->school_id, '1/0']));

        $response->assertStatus(200)
                ->assertViewHas('users')
                ->assertSeeText(e($student->student_code));
    }

    /**
     * @test
     */
    public function canSeeSomePersonalStudentInformation()
    {
        $student = factory(User::class)->states('student')->create();
        $response = $this->get(url('/user', [$student->student_code]));

        $response->assertStatus(200)
                ->assertSeeText(e($student->student_code))
                ->assertSeeText(e($student->blood_group));
    }

    /**
     * @test
     */
    public function canSeeListOfTeachers()
    {
        $teacher = factory(User::class)->states('teacher')->create();
        $response = $this->get(url('/users', [$this->librarian->school_id, '0/1']));

        $response->assertStatus(200)
                ->assertViewHas('users')
                ->assertSeeText(e($teacher->student_code));
    }

    /**
     * @test
     */
    public function canSeeSomePersonalTeacherInformation()
    {
        $teacher = factory(User::class)->states('teacher')->create();
        $response = $this->get(url('/user', [$teacher->student_code]));

        $response->assertStatus(200)
                ->assertSeeText(e($teacher->student_code))
                ->assertSeeText(e($teacher->nationality))
                ->assertDontSeeText('Blood');
    }

    /**
     * @test
     */
    public function canSeeListOfLibrarians()
    {
        $librarian1 = factory(User::class)->states('librarian')->create();
        $librarian2 = factory(User::class)->states('librarian')->create();
        $response = $this->get(url('/users', [$this->librarian->school_id, 'librarian']));

        $response->assertStatus(200)
                ->assertViewHas('users')
                ->assertSeeText(e($librarian1->student_code))
                ->assertSeeText(e($librarian2->student_code));
    }

    /**
     * @test
     */
    public function canSeeSomePersonalLibrarianInformation()
    {
        $librarian1 = factory(User::class)->states('librarian')->create();
        $response = $this->get(url('/user', [$librarian1->student_code]));

        $response->assertStatus(200)
                ->assertSeeText(e($librarian1->name))
                ->assertSeeText(e($librarian1->nationality))
                ->assertDontSeeText('Blood');
    }

    /**
     * @test
     */
    public function canSeeAllBooks()
    {
        $books = factory(Book::class, 4)->create();
        $response = $this->get(route('library.books.index'));

        $response->assertStatus(200)
                ->assertViewHas('books');

        $books->each(function ($book) use ($response) {
            $response->assertSeeText(e($book->title));
        });
    }

    /**
     * @test
     */
    public function canSeeBookDetails()
    {
        $book = factory(Book::class)->create();

        $this->get(route('library.books.show', $book))
            ->assertViewHas('book')
            ->assertSeeText(e($book->title));
    }

    /**
     * @test
     */
    public function canEditBookDetails()
    {
        $book = factory(Book::class)->create(['title' => 'Foo Bar']);

        $this->get(route('library.books.edit', $book))
            ->assertViewHas('book')
            ->assertSeeText('Update Book Info');
    }

    /**
     * @test
     */
    public function canSeeIssuedBooks()
    {
        $issuedBook = factory(Issuedbook::class)->create();

        $response = $this->get(route('library.issued-books.index'))
                        ->assertViewHas('issued_books')
                        ->assertSeeText((string) $issuedBook->book_id);
    }

    /**
     * @test
     */
    public function canAddANewBook()
    {
        $this->get(route('library.books.create'))
            ->assertStatus(200)
            ->assertSeeText('Save');
    }
}
