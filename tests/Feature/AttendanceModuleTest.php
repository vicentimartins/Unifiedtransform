<?php

namespace Tests\Feature;

use App\Exam;
use App\User;
use App\Course;
use App\Section;
use App\Attendance;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AttendanceModuleTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $admin = factory(User::class)->states('admin')->create();
        $this->actingAs($admin);
        $this->withoutExceptionHandling();
    }

    /** @test */
    public function canViewStudentAttendanceBySection()
    {
        $section = factory(Section::class)->create();
        $exam = factory(Exam::class)->create();
        $response = $this->get('attendances/'.$section->id.'/0/'.$exam->id);
        $response->assertStatus(200);
        $response->assertViewIs('attendance.attendance');
        $response->assertViewHas('attendances');
    }

    /** @test */
    public function canViewStudentsBySection()
    {
        $section = factory(Section::class)->create();
        $response = $this->get('attendances/'.$section->id);
        $response->assertStatus(200);
        $response->assertViewIs('list.student-list');
        $response->assertViewHas('users');
    }

    /** @test */
    public function studentsAreAddedToASectionBeforeTakingAttendance()
    {
        $teacher = factory(User::class)->states('teacher')->create();
        $section = factory(Section::class)->create();
        $exam = factory(Exam::class)->create();
        $course = factory(Course::class)->create();
        $response = $this->get('attendances/students/'.$teacher->id.'/'.$course->id.'/'.$exam->id.'/'.$section->id);
        $response->assertStatus(200);
        $response->assertViewIs('attendance.attendance');
        $response->assertViewHas([
            'students',
            'attendances',
            'attCount',
            'section_id',
            'exam_id',
        ]);
    }

    /** @test */
    public function viewStudentAttendanceByStudentId()
    {
        $student = factory(User::class)->states('student')->create();
        $response = $this->get('attendances/0/'.$student->id.'/0');
        $response->assertStatus(200);
        $response->assertViewIs('attendance.student-attendances');
        $response->assertViewHas('attendances');
    }

    /** @test */
    public function adminOrTeacherCanViewAdjustStudentAttendanceForm()
    {
        $student = factory(User::class)->states('student')->create();
        $response = $this->get('attendance/adjust/'.$student->id);
        $response->assertStatus(200);
        $response->assertViewIs('attendance.adjust');
        $response->assertViewHas('attendances');
    }

    /** @test */
    public function adminOrTeacherCanAdjustStudentAttendance()
    {
        $request = [
            'att_id' => [1, 3, 5],
            'isPresent' => [1, 0, 1],
        ];
        factory(Attendance::class, 5)->create();
        $response = $this->followingRedirects()->post('attendance/adjust', $request);
        $response->assertStatus(200);
    }

    /** @test */
    public function adminOrTeacherCanTakeStudentsAttendance()
    {
        $attendances = factory(Attendance::class, 5)->create();
        $request = [
            'students' => [
                $attendances[0]->student_id,
                $attendances[1]->student_id,
                $attendances[2]->student_id,
            ],
            'attendances' => [0],
            'section_id' => $attendances[0]->section_id,
            'exam_id' => $attendances[0]->exam_id,
            'update' => 0,
        ];

        $response = $this->followingRedirects()->post('attendance/take-attendance', $request);
        $response->assertStatus(200);
    }

    /** @test */
    public function adminOrTeacherCanUpdateStudentsAttendance()
    {
        $attendances = factory(Attendance::class, 5)->create();
        $request = [
            'students' => [
                $attendances[0]->student_id,
                $attendances[1]->student_id,
                $attendances[2]->student_id,
            ],
            'attendances' => [1, 2, 3],
            'section_id' => $attendances[0]->section_id,
            'exam_id' => $attendances[0]->exam_id,
            'update' => 1,
        ];

        $response = $this->followingRedirects()->post('attendance/take-attendance', $request);
        $response->assertStatus(200);
    }
}
