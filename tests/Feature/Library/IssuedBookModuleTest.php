<?php

namespace Tests\Feature\Library;

use App\User;
use App\Issuedbook;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IssuedBookModuleTest extends TestCase
{
    use RefreshDatabase;

    protected $librarian;

    public function setUp(): void
    {
        parent::setUp();
        $this->librarian = factory(User::class)->states('librarian')->create();
        $this->actingAs($this->librarian);
        $this->withoutExceptionHandling();
    }

    /** @test */
    public function librarianCanIssueBooks()
    {
        $request = factory(Issuedbook::class)->make([
            'book_id' => [1, 2, 3, 4, 5],
        ]);
        $this->followingRedirects()->post('library/issue-books', $request->toArray())
            ->assertStatus(200);
        $this->assertEquals(Issuedbook::count(), count((array) $request->book_id));
    }
}
