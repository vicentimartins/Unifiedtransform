<?php

namespace App\Listeners;

use App\Services\User\UserService;
use App\Events\StudentInfoUpdateRequested;

class UpdateStudentInfo
{
    protected $userService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(StudentInfoUpdateRequested $event)
    {
        try {
            $this->userService->updateStudentInfo($event->request, $event->student_id);

            return true;
        } catch (\Exception $ex) {
            Log::info('Failed to update Student information, Id: '.$event->user_id);

            return false;
        }
    }
}
