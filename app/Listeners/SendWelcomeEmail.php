<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use App\Mail\SendWelcomeEmailToUser;
use Illuminate\Support\Facades\Mail;

class SendWelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        Mail::to($event->user)->send(new SendWelcomeEmailToUser($event->user, $event->password));
    }
}
