<?php

namespace App;

class Homework extends Model
{
    protected $table = 'homeworks';

    /**
     * Get the teacher record associated with the user.
     */
    public function teacher()
    {
        return $this->belongsTo('App\User');
    }
}
