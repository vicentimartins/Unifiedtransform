<?php

namespace App;

class StudentInfo extends Model
{
    protected $table = 'student_infos';
    protected $fillable = ['student_id'];

    /**
     * Get the student record associated with the user.
     */
    public function student()
    {
        return $this->belongsTo('App\User');
    }
}
